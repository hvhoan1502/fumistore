// Import modules
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { APP_BASE_HREF } from '@angular/common';
import { HttpClientModule } from'@angular/common/http';

//Import component modules
import { HomeModule } from './modules/public/home/home.module';

import { FooterComponent } from './modules/shared/footer/footer.component';
import { HeaderComponent } from './modules/shared/header/header.component';

// Import router modules
import { AppRoutingModule } from './modules/app-routing.module';

// Import home components
import { HomeComponent } from './modules/public/home/home.component'
import { from } from 'rxjs';

@NgModule({
  declarations: [
    FooterComponent,
    HeaderComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HomeModule,
    AppRoutingModule
  ],
  providers: [
    {provide: APP_BASE_HREF, useValue: '/'}
  ],
  bootstrap: [ HomeComponent ]
})
export class AppModule { }
