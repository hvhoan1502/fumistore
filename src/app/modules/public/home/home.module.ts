import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SliderComponent } from './slider/slider.component';
import { NewProductComponent } from './new-product/new-product.component';
import { HomeComponent } from './home.component';

@NgModule({
  declarations: [
    SliderComponent,
    NewProductComponent,
    HomeComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    SliderComponent
  ]
})
export class HomeModule { }
