import { NgModule } from '@angular/core';
import { RouterModule, Routes} from '@angular/router'

import { HomeComponent } from './public/home/home.component';


const router: Routes = [
  { path: '', redirectTo: '/index', pathMatch: 'full' },
  { path: 'index', component: HomeComponent}
]

@NgModule({
  imports: [RouterModule.forRoot(router)],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
