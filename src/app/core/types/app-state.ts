export interface User {
    _id: string;
    name: string;
    email: string;
}

export interface AppState {
    user: User;
    loading: boolean;
}