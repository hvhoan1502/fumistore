import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { environment } from '../../../environments/environment'
import { HttpClient, HttpHeaders, HttpResponse, HttpErrorResponse } from '@angular/common/http';

const SERVER_URL = environment.apiUrl;

@Injectable()

export class RequestService {
    constructor(private httpClient: HttpClient) {}

    processResponse(response: any) {
        return response.then(data => data)
                       .catch(err => this.errorHandler(err));
    }

    errorHandler(error: HttpErrorResponse) {
        return Observable.throw(error.message || 'Server error')
    }

    get(subUrl: string) {
        const headers = new HttpHeaders({ token: localStorage.getItem('token') });
        const response = this.httpClient.get(SERVER_URL + subUrl, { headers }).toPromise();
        return this.processResponse(response);
    }

    post(subUrl: string, body) {
        const headers = new HttpHeaders({ token: localStorage.getItem('token') });
        const response = this.httpClient.post(SERVER_URL + subUrl, body, { headers }).toPromise();
        return this.processResponse(response);
    }

    put(subUrl: string, body) {
        const headers = new HttpHeaders({ token: localStorage.getItem('token') });
        const response = this.httpClient.put(SERVER_URL + subUrl, body, { headers }).toPromise();
        return this.processResponse(response);
    }

    delete(subUrl: string) {
        const headers = new HttpHeaders({ token: localStorage.getItem('token') });
        const response = this.httpClient.delete(SERVER_URL + subUrl, { headers }).toPromise();
        return this.processResponse(response);
    }
}
